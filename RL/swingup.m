function [par, ta, xa] = swingup(par)

    par.simtime = 10;     % Trial length
    par.simstep = 0.05;   % Simulation time step
    par.maxtorque = 1.5;  % Maximum applicable torque
    
    
    if strcmp(par.run_type, 'learn')
        %%
        % Obtain SARSA parameters
        par = get_parameters(par);
        
		% TODO: Initialize the outer loop
        Q = init_Q(par);

        % Initialize bookkeeping (for plotting only)
        ra = zeros(par.trials, 1);
        tta = zeros(par.trials, 1);
        te = 0;

        % Outer loop: trials
        for ii = 1:par.trials

            % TODO: Initialize the inner loop
            x = swingup_initial_state();            % choose random starting point
            s = discretize_state(x, par);           % Convert to discrete state
            a = execute_policy(Q, s, par);          % Find first action

            % Inner loop: simulation steps
            for tt = 1:ceil(par.simtime/par.simstep)
                
                % TODO: obtain torque
                u = take_action(a, par);
                
                % Apply torque and obtain new state
                % x  : state (input at time t and output at time t+par.simstep)
                % u  : torque
                % te : new time
                [te, x] = body_straight([te te+par.simstep],x,u,par);

                % TODO: learn
                % use s for discretized state
                sP = discretize_state(x, par);
                aP = execute_policy(Q, s, par);
                r = observe_reward(a, sP, par);
                Q = update_Q(Q, s, a, r, sP, aP, par);
                
                % update states for next 
                s = sP;
                a = aP;
                
                % Keep track of cumulative reward
                ra(ii) = ra(ii)+r;

                % TODO: check termination condition
                if (is_terminal(s, par) == 1)
                    break;
                end
            end

            tta(ii) = tta(ii) + tt*par.simstep;

            % Update plot every ten trials
            if rem(ii, 10) == 0
                plot_Q(Q, par, ra, tta, ii);
                drawnow;
            end
        end
        
        % save learned Q value function
        par.Q = Q;
        disp("Q is saved");
 
    elseif strcmp(par.run_type, 'test')
        %%
        % Obtain SARSA parameters
        par = get_parameters(par);
        
        % Read value function
        Q = par.Q;
        
        x = swingup_initial_state();
        
        ta = zeros(length(0:par.simstep:par.simtime), 1);
        xa = zeros(numel(ta), numel(x));
        te = 0;
        
        % Initialize a new trial
        s = discretize_state(x, par);
        a = execute_policy(Q, s, par);

        % Inner loop: simulation steps
        for tt = 1:ceil(par.simtime/par.simstep)
            % Take the chosen action
            TD = max(min(take_action(a, par), par.maxtorque), -par.maxtorque);

            % Simulate a time step
            [te,x] = body_straight([te te+par.simstep],x,TD,par);

            % Save trace
            ta(tt) = te;
            xa(tt, :) = x;

            s = discretize_state(x, par);
            a = execute_policy(Q, s, par);

            % Stop trial if state is terminal
            if is_terminal(s, par)
                break
            end
        end

        ta = ta(1:tt);
        xa = xa(1:tt, :);
        
    elseif strcmp(par.run_type, 'verify')
        %%
        % Get pointers to functions
        learner.get_parameters = @get_parameters;
        learner.init_Q = @init_Q;
        learner.discretize_state = @discretize_state;
        learner.execute_policy = @execute_policy;
        learner.observe_reward = @observe_reward;
        learner.is_terminal = @is_terminal;
        learner.update_Q = @update_Q;
        learner.take_action = @take_action;
        par.learner = learner;
    end
    
end

% ******************************************************************
% *** Edit below this line                                       ***
% ******************************************************************
function par = get_parameters(par)
    % TODO: set the values
    par.epsilon = 0.1;        % Random action rate
    par.gamma = 0.99;         % Discount rate
    par.alpha = 0.25;          % Learning rate
    par.pos_states = 31;      % Position discretization
    par.vel_states = 31;      % Velocity discretization
    par.actions = 5;          % Action discretization
    par.trials = 2000;        % Learning trials

    % self added
    par.r_max = 1;            % maximum reward
    par.Q_0 = 0;           % initial value of Q states
end

function Q = init_Q(par)
    % TODO: Initialize the Q table.
    Q = par.Q_0/par.r_max * ones(par.pos_states, par.vel_states, par.actions);
    
end

function s = discretize_state(x, par)
    % TODO: Discretize state. Note: s(1) should be
    % TODO: position, s(2) velocity.
    s(1) = round( mod(x(1), 2*pi ) /2/pi * (par.pos_states-1)) +1;
    x2 = min(x(2), 4.9999*pi);
    x2 = max(x2, -5*pi);
    s(2) = round( mod(x2+ (5*pi), 10*pi)/10/pi * (par.vel_states-1)) +1;
    
end

function u = take_action(a, par)
    % TODO: Calculate the proper torque for action a. This cannot
    % TODO: exceed par.maxtorque.
    u = (a-1)/(par.actions-1) * 2 * par.maxtorque - par.maxtorque;
end

function r = observe_reward(a, sP, par)
    % TODO: Calculate the reward for taking action a,
    % TODO: resulting in state sP
    diff_pos = abs(sP(1) - round(par.pos_states/2));
    diff_vel = abs(sP(2) - round(par.vel_states/2));
    if abs(diff_pos) == abs(diff_vel)
        r_add = 1 / (1+abs(diff_pos*diff_vel))^2;
    else; r_add = 0; end;
    r_add = 0; % for normal optimistic value test
    r = par.r_max * max( (1-diff_pos-diff_vel) , 0) + r_add;
end

function t = is_terminal(sP, par)
    % TODO: Return 1 if state sP is terminal, 0 otherwise.
    diff_pos = abs(sP(1) - round(par.pos_states/2));
    diff_vel = abs(sP(2) - round(par.vel_states/2));
    t = max( (1-diff_pos-diff_vel) , 0);
end


function a = execute_policy(Q, s, par)
    % TODO: Select an action for state s using the
    % TODO: epsilon-greedy algorithm.
    current_state = Q(s(1), s(2), :); % current state array
    [ Q_value, index ] = max(current_state);
    
    % Randomly use a random action with a weighted change
    if rand(1) < par.epsilon
        a = randi(par.actions); % choose random 
    else
        a = index;
    end
end

function Q = update_Q(Q, s, a, r, sP, aP, par)
    % TODO: Implement the SARSA update rule.
    change = par.alpha*( r + par.gamma*Q(sP(1),sP(2),aP) - Q(s(1),s(2),a));
    Q(s(1),s(2),a) = Q(s(1),s(2),a) + change;
end

