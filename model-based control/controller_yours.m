clc
clearvars
close all

% tau - torques applied to joints
% th - positions of the joints (angles)
% th_d - velocities of the joints (angular velocity)
% th_dd - acceleration of the joints
% _des - desired values (reference)
% _curr - current values (measured)
% ff_ - feedforward
% fb_ - feedback

rp = define_robot_parameters();
sim_time = 10; % simualtion time in seconds
dt = 0.03; % time difference in seconds
t = 0:dt:sim_time;

%% DESIRED TRAJECTORY DATA
d2r  = pi/180;             % degrees to radians
tp.w_default = 72*d2r;            % rotational velocity rad/s
tp.rx = 1.75; tp.ry = 1.25; % ellipse radii
tp.ell_an = 45*d2r;       % angle of inclination of ellipse
tp.x0 = 0.4;  tp.y0 = 0.4;  % center of ellipse  

% generate training data tables
input_data = [];
target_data = [];

% set range of training data
iterations = 15;
rot_velocity = 70:(10/(iterations-1)):80;
for iter = 1:iterations
    % iterate over rotational velocity
    tp.w = rot_velocity(iter)*d2r;
    % Calculate desired trajectory in task space and in joint space
    des = calculate_trajectory(t, tp, rp);

    th_0 = des.th(:,1) - [0.1; 0.2];
    th_d_0 = des.th_d(:,1);

    %% TRAIN INV MODEL
    curr.th    = zeros(2,length(t));
    curr.th_d  = zeros(2,length(t));

    curr.th(:,1)   = th_0;
    curr.th_d(:,1) = th_d_0;
    [curr.x(:,1), curr.x_d(:,1), curr.x_dd(:,1), curr.x_eb(:,1)] = FK(th_0, th_d_0, [0; 0], rp);

    target_data_iter = zeros(2,length(t));
    for iter = 2:length(t)
        th_curr = curr.th(:,iter-1); th_d_curr = curr.th_d(:,iter-1);
        th_des = des.th(:,iter); th_d_des = des.th_d(:,iter); th_dd_des = des.th_dd(:,iter);
        [M_des, V_des, G_des] = RBD_matrices(th_des,rp);
        target_data_iter(:, iter) = (M_des*(th_dd_des) + V_des*(th_d_des.^2) + G_des)';
    end
    
    % append new data to training set
    input_data_iter = [des.th; des.th_d; des.th_dd];
    input_data = [input_data, input_data_iter];
    target_data = [target_data, target_data_iter];
    
end
net = feedforwardnet([10,10])
% train the neural net on all data sets
net = train(net, input_data, target_data);
view(net)
save net


%% SIMULATE ROBOT
Kp = [500; 500];
Kd = [50; 50];
curr = simulate_robot_yours(t, dt, th_0, th_d_0, des, rp, ...
    @(th_curr, th_d_curr, th_des, th_d_des, th_dd_des) ff_yours(th_curr, th_d_curr, th_des, th_d_des, th_dd_des, net), ...
    @(th_curr, th_d_curr, th_des, th_d_des) fb_pd(th_curr, th_d_curr, th_des, th_d_des, Kp, Kd));

robot_animation(t, curr, des);
analyze_performance(t, curr, des);